Developer : Asit 

email : asitishere@yahoo.com

Before you run the test :

Note :

The test is executing in MAC ( iOS) but added check for Windows and extra check can be added for Linux

Install and set path Java 1.8 or higher Install maven 3.3.2 or higher and set to path

Open the project in Intellij Idea or any JAVA IDE ( Maven will auto import the dependencies)


The test run in Parallel threads , its upto the executor to select how many browser instance we want to spin up.This can be controlled by changing the below parameters in POM.xml. Later we can pass as a property flag during execution.

        <configuration>
            <forkCount>3</forkCount>
            <testFailureIgnore>true</testFailureIgnore>
        </configuration>
To execute test please use command "mvn clean verify"

to check the report after execution open /target folder and open the report html file from below location

${project.build.directory}/Qantas-REST API Automation-report

Design Pattern

Page Object Design pattern .

CI Execution

The test is a maven build and easily gets integrated to any CI
