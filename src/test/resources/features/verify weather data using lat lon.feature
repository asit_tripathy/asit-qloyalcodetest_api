Feature: Current Weather Data for multiple places in the world based on Lat and Lon

  @GetByLatLong
  Scenario Outline: Verify weather
    Given The customer has API key to access weather data
    And The customer set the valid Latitude as <lat> and Longitude as <lon>
    When The customer send a GET request to get the current weather data
    Then The customer should get the weather data success API response


    Examples:
      |lat        |lon        |
      |33.8059    |150.9702   |
      | 36.7783   |119.4179   |
      |33.8688    |151.2093   |