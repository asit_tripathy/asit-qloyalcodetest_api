Feature: Current Weather Data for multiple places in the world based on PostCode

  @GetByPostCode
  Scenario Outline: Verify Current Weather Data for multiple places in the world based on PostCode
    Given The customer has API key to access weather data
    And The customer set the valid post code as <Post Code>
    Then The customer should get the weather data success API response


    Examples:
    |Post Code|
    | 299501       |
    |85055         |
    |72217         |