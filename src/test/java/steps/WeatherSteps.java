package steps;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import objectMappers.WeatherObj;
import org.testng.Assert;
import pages.WeatherPage;
import properties.GlobalConstants;

public class WeatherSteps {

    String apiKey;
    String lat;
    String lon;
    String postCode;
    Response res;


    @Given("The customer has API key to access weather data")
    public void user_has_api_key(){
        apiKey= GlobalConstants.WEATHER_API_KEY;
    }

    @And("The customer set the valid Latitude as (.*) and Longitude as (.*)")
    public void setLatAndLon(String lat,String lon){
        this.lat = lat;
        this.lon= lon;
    }

    @When("The customer send a GET request to get the current weather data")
    public void getWeatherDataUsingLatLonAndAPIKey(){
       res= new WeatherPage().requestWeatherDataWithLatLonAndAPIKey(lat,lon);
    }

    //@JsonProperty("wrapper")


    @Then("The customer should get the weather data success API response")
    public void verifyWeatherDataResponse() throws JsonProcessingException {
       Assert.assertEquals(res.statusCode(),200,"Response code verification");


/*        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        WeatherObj weatherObj = objectMapper.readValue(res.prettyPrint(), WeatherObj.class);
        weatherObj.getLat();*/

    }

    @And("The customer set the valid post code as (.*)")
    public void setPostCode(String postCode){
        res= new WeatherPage().requestWeatherDataByPostCode(postCode);
    }


}
