package pages;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import properties.GlobalConstants;

import static io.restassured.RestAssured.given;

public class WeatherPage {

    public Response requestWeatherDataWithLatLonAndAPIKey(String lat, String lon){

        return given()
                .when()
                .get(GlobalConstants.WEATHER_API_ENDPOINT+"current?lat="+lat+"&lon="+lon+"&key="+GlobalConstants.WEATHER_API_KEY);
    }

    public Response requestWeatherDataByPostCode(String postCode){
        return given()
                .when()
                .get(GlobalConstants.WEATHER_API_ENDPOINT+"current?postal_code="+postCode+"&key="+GlobalConstants.WEATHER_API_KEY);
    }


}
